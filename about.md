---
layout: page
title: About
permalink: /about/
---

<amp-img width="600" height="300" layout="responsive" src="http://lorempixel.com/600/300/sports"></amp-img>
<center>
<h1>Corso di Aikido Dentoo Iwama Ryu a Siracusa</h1>
    <p>
        <span>Il corso è tenuto dal 11 Novembre 2021 presso:</span><br>
        <span><a href="https://goo.gl/maps/yYBKn3GP85fsTwTA8">Palestra Sagittario</a> di via di Villa Ortisi, 32/8 a Siracusa.</span><br>
        <span>Martedì e Giovedì dalle 14:00 alle 15:30</span>
    </p>
</center>

<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3182.938863412823!2d15.271645315412714!3d37.08276295944222!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1313ce8e9c94b5e5%3A0x585f478c9516e958!2sPalestra%20Sagittario!5e0!3m2!1sit!2sit!4v1636889921843!5m2!1sit!2sit' width='600' height='450' style='border:0;' allowfullscreen='' loading='lazy'></iframe></div>