---
layout: page
title: O Sensei Morihei Ueshiba
permalink: /osenseimoriheiueshiba/
---

O Sensei Morihei Ueshiba

1883: Nasce a TANABE, Provincia di Kii (Prefettura di Wakayama) il 14 Dicembre.

1901: (17 anni) Si trasferisce a Tokyo nel mese di Settembre e apre un negozio di articoli di cancelleria. Studia "Tenshin Shin’yo-ryu Jujitsu" con il Maestro Tokusaburo Tozawa a Tokyo. Breve studio della scuola di spada Shinkage-ryu.

1903: (19 anni) Sposa Hatsu Itogawa a Tanabe. Alla fine di Dicembre si arruola nell’Esercito: 61° Reggimento di Fanteria di Wakayama.

1904: (20 anni) Scoppia la guerra tra la Russia ed il Giappone.

1905: (21 anni) Partenza del suo Reggimento per il fronte in Mancuria.

1906: (22 anni) Si congeda dall’Esercito e ritorna a Tanabe.

1908: (24 anni) Riceve dal Maestro Masanosuke Tsuboi (oppure da Masakatsu Nakai) il certificato di "Yagyu-ryu Jujitsu".

1910: (26 anni) Si sposta nell’ "Hokkaido" per valutare un progetto di sviluppo del territorio sponsorizzato dal governo. Torna subito a Tanabe.

1911: (27 anni) È possibile un suo approccio allo studio del "Judo" con il Maestro Kiyoichi Takagi a Tanabe.
Nasce la prima figlia, Matsuko.

1912: (28 anni) Comanda un gruppo di coloni che dalla Provincia di Kii decidono di stabilirsi su nell’Hokkaido, in Aza-Shirataki, villaggio di Kamiwakibetsu, nella Contea Mombetsu.

1915: (31 anni) Incontra" Sokaku Takeda" del "Daito-ryu Jujitsu" e nel mese di Marzo partecipa al suo primo seminario di 10 giorni.

1916: (32 anni) Studia intensamente Daito-ryu Aikijujutsu.

1917: (33 anni) Nasce a Luglio il primo figlio maschio, Takemori.

1918: (34 anni) È consigliere comunale nel villaggio di Kamiwakibetsu dal Giugno 1918 ad Aprile 1919.

1919: (35 anni) In Dicembre lascia Hokkaido per tornare dal padre malato. Trasferisce la terra e le proprietà al suo Maestro, Sokaku Takeda.

1920: (36 anni) Incontra ad Ayabe "Onisaburo Deguchi" della religione "Omoto".
Quando arriva a Tanabe il padre, Yoroku, è appena morto.
Si trasferisce con la famiglia ad Ayabe, nella Prefettura di Kyoto, centro della Religione Omoto.
Costruisce, attaccato alla sua casa, il dojo "Ueshiba Juku".
Nasce ad Aprile il secondo maschio, Kuniharu.
Ad Agosto muore il primo figlio, Takemori.
A Settembre muore anche il secondo figlio, Kuniharu.

1921: (37anni) Avviene il primo incidente "Omoto".
A Giugno nasce il terzo maschio, Kisshomaru ( Koetsu ).

1922: (38anni) Muore la madre, Yuki.
Sokaku Takeda e famiglia si fermano ad Ayabe dal 28 Aprile al 15 Settembre.
A Settembre riceve da Takeda il "KYOJU DAIRI", la qualifica di istruttore di Daito-ryu.

1924: (40 anni) Nel periodo compreso tra Febbraio e Luglio va in Mongolia, insieme ad Onisaburo Deguchi, con l’obbiettivo di fondare una comunità utopistica. Catturato ed imprigionato dall’esercito Cinese, riesce a scampare al plotone di esecuzione.

1925: (41 anni) Tiene una dimostrazione a Tokyo davanti ai più alti ufficiali militari e dignitari di corte.

1927: (43 anni) Si trasferisce a Tokyo con tutta la famiglia.
Allestisce un dojo provvisorio nella sala da biliardo del palazzo del Conte Shimazu a Shiba, Shirogane in Sarumachi.

1928: (44 anni) Si sposta a Shiba, Tsunamachi, dojo temporaneo.

1929: (45 anni) Si trasferisce con la famiglia a Shiba, Kuruma-machi, dove sistema un dojo momentaneo.

1930: (46 anni) Si trasferisce a Shimo-ochiai in Mejiro.
Jigoro Kano, fondatore del Judo, dopo aver osservato una sua dimostrazione nel "Mejiro dojo" invia molti studenti del Kodokan a studiare da Ueshiba Sensei, tra cui Minoru Mochizuki.

1931: (47 anni) Inaugurazione del "KOBUKAN" dojo a Ushigome, Wakamatsu-cho.
Sokaku Takeda insegna dal 20 Marzo al 7 diAprile presso il Kobukan dojo. Per l’ultima volta il nome di Ueshiba appare tra gli iscritti sul libro di Takeda.

1932: (48 anni) Si stabilisce il "Budo Sen’yokai" (Società per la Promozione delle Arti Marziali ) con a capo Ueshiba Sensei.

1933: (49 anni) Il manuale tecnico "BUDO RENSHU" viene pubblicato.

1935: (51 anni) Ad Osaka, partecipa ad un documentario filmato dal giornale Asahi con Takuma Hisa. Avviene il secondo incidente "Omoto".

1937: (53 anni) Il nome di Morihei Ueshiba appare, insieme a quello di Zenzaburo Akazawa, sul libro delle iscrizioni della scuola "Kashima Shinto-ryu".

1938: (54 anni) Viene pubblicato il manuale tecnico "BUDO".

1939: (55 anni) Viene invitato in Manchuria ad insegnare.

1940: (56 anni) Partecipa ad una dimostrazione di Arti Marziali in Manchuria che commemora il 2600° anniversario del Giappone.

1941: (57 anni) Tiene una dimostrazione, organizzata dall’Ammiraglio Isamu Takeshita, di fronte ai membri della famiglia imperiale.
Insegna all’Accademia Militare di Polizia.
Viene invitato ad insegnare in Manchuria in occasione della Settimana Universitaria delle Arti Marziali.
Diventa consulente per le Arti Marziali nelle Università di Shimbuden e Kenkoru.

1942: (58 anni) Appare ufficialmente il nome "AIKIDO" registrato dal Ministero dell’Educazione.
In Agosto, in occasione del 10° anniversario dell’indipendenza, viene invitato, in Manchuria, a tenere una dimostrazione come rappresentante delle Arti Marziali Giapponesi.
SI TRASFERISCE AD IWAMA, PREFETTURA DI IBARAGI.
Suo figlio, Kisshomaru Ueshiba, diventa direttore del Kobukan.

1943: (59 anni) Costruisce ad Iwama il primo "AIKI JINJIA".

1945: (61 anni) Con la fine della 2° guerra mondiale e la messa al bando delle arti marziali la Fondazione Kobukai cessa la sua attività.
IL DOJO DI IWAMA È COMPLETATO.

1946: (62 anni) Morihiro Saito entra nel dojo di Iwama.

1948: (64 anni) In Iwama è immerso totalmente nei lavori dei campi, nella pratica e nella meditazione.
L’Aikikai Honbu Dojo si trasferisce ad Iwama, un ufficio rimane aperto a Tokyo. Kisshomaru diventa il capo dell’Aikikai dojo.

1949: (65 anni) Al dojo di Tokyo riprende la pratica regolare.

1956: (72 anni) L’Hombu ritorna a Tokyo.
"O Sensei Morirei Ueshiba" tiene una dimostrazione all’Aikikai Hombu Dojo di fronte a molti dignitari stranieri, organizzata da Andrè Nocquet.

1958: (74 anni) Appare in un documentario della televisione U.S.

1960: (76 anni) Riceve la Medaglia d’Onore dal Governo Giapponese.

1961: (77 anni) A Febbraio va nelle Hawaii invitato all’inaugurazione della "Honolulu Aikikai". Appare in un documentario televisivo della NHK.
La "All-Japan Student Aikido Federation" viene stabilita con Ueshiba presidente.

1963: (79 anni) In Ottobre si tiene, a Hibiya Kokaido, per la prima volta una dimostrazione della All Japan Aikido.

1964: (80 anni) Riceve, come fondatore dell’Aikido, l’Ordine del Sole Nascente, 4° Classe.

1968: (84 anni) Viene costruito il nuovo Hombu Dojo.

1969: (85 anni) Appare nella sua ultima dimostrazione il 15 Gennaio durante la Celebrazione del Kagami Biraki.
Muore il 26 Aprile per un cancro al fegato. Le ceneri vengono seppellite a Kozanji, Tanabe. I capelli vengono conservati ad Iwama, Kumano, Ayabe e all’Hombu Dojo.
Diventa cittadino onorario di Tanabe e di Iwama.
A Giugno muore anche sua moglie, Hatsu.
