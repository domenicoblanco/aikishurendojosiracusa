---
layout: page
title: M. Alessandro Tittarelli
permalink: /malessandrotittarelli/
---

7° Dan Dentoo Iwama Ryu

Inizia la pratica del Budo nel 1973 con Judo e Karate.
Nel 1976 inizia la pratica dell'Aikido.
Nel 1984 inizia lo studio del Takemusu Aiki Iwama Style.
Nel 1985 incontra Saito Morihiro Shihan.
Nel 1991 incontra Saito Hitohira Sensei.
Dal 1992 va periodicamente ad Iwama, in Giappone.
Dal 1984 segue gli insegnamenti della famiglia Saito ed, attualmente, è il Responsabile Nazionale per l'Italia della Iwama Shin Shin Aiki Shurenkai.
Dal 1982 è sposato con Gabriella. Ha tre figli, Laura, Francesca e Febo. Oltre che Maestro di Aikido è anche un Operatore Craniosacrale iscritto all'AITeCS



Biografia
1976: Inizia la pratica dell’Aikido.

Alessandro Tittarelli nasce il 25 Agosto 1957 a Jesi, in provincia di Ancona. Fin da giovanissimo si appassiona alle arti marziali. Nel 1973 si iscrive presso l’unica palestra dove sia possibile praticare un’arte marziale a Jesi: Il Circolo Cittadino, in Via Mura Orientali, dove insegna il M° Aldo Natalini, allora cintura nera di Judo. Pratica per due anni Judo e poi per un anno Karate Shotokan.

Nel 1976, sempre presso i locali del Circolo Cittadino di Jesi, apre un corso di Aikido il giovane Paolo Corallini, con il nome di KOBUKAN DOJO. Paolo Corallini sta studiando Ju-Jutsu e Aikido a Macerata, sotto la guida del M° Adone Giorio, già da alcuni anni. I giovani Alessandro e Paolo risiedono nello stesso paese, Filottrano, in provincia di Ancona e si conoscono e si frequentano già da tempo. Comincia così un’avventura nel mondo dell’Aikido che porterà Alessandro Tittarelli ad essere a stretto contatto con il M° Paolo Corallini, per i successivi 23 anni (fino all’Aprile del 1998) ed a partecipare, con lui, a più di 350 seminari in giro per l’Italia, l’Europa ed il resto del mondo. I primi anni di pratica sono molto duri perché mancano strutture ed organizzazioni adeguate alla diffusione di quest’arte marziale. L’unica possibilità rimane quella di andare fuori, anche all’estero e partecipare ai seminari di quanti più Maestri possibili: Dai vari Filippini, Kawamukai, Tada, Kobayashi, Tamura, Tohei, Yamaguchi, André Noquet.

1980: Inizia ad insegnare

Nel Settembre del 1980 Alessandro apre il suo primo corso di Aikido. Insegna presso il "Ken Otani Dojo" di Filottrano (AN) dove il M° Lelio Accorroni tiene un corso di Judo. Una prima classe è per bambini fino a 12 anni ed una seconda è per adulti. Questi sono gli anni in cui si fonda l’Unione Italiana Aikido con Presidente Paolo Corallini; associazione legata all’Unione Europea Aikido diretta dal Maestro André Noquet.

Il 29 Novembre 1981 ottiene il 1° Dan U.I.A. (Unione Italiana Aikido)

Nel 1982 apre un altro corso a Tolentino (MC) presso i locali del Tayton Club.
Il nome del dojo è "AIKI SHRINE DOJO" ("Aiki-jinja Dojo"), un nome al quale Alessandro rimarrà fortemente legato fino al 1998, quando lo cambierà in "AIKI SHUREN DOJO – ANCONA".

Nel 1983 costruisce un nuovo dojo a Filottrano (AN) in Via Gemme sempre con il nome "Aiki Shrine Dojo" e vi sposta il corso che si teneva prima al "Ken Otani Dojo".

Il 20 Novembre 1983 ottiene il 2° Dan U.I.A. in occasione di un seminario internazionale di Aikido con il Maestro André Noquet, organizzato a Filottrano (AN) presso l’hotel Sette Colli.

1984: Inizia a seguire gli insegnamenti di Saito Sensei

Nel 1984 apre un corso a Macerata presso il "Samurai Budo" ed in giorni diversi si sposta anche ad Ancona dove apre un corso di Aikido presso i locali del "Busen Dojo", vicino al vecchio Stadio Dorico.

Nel Maggio 1984 il M° Corallini va per la prima volta in Giappone, ad Iwama, nel Dojo del Fondatore per un soggiorno di tre settimane, dove incontra il Maestro Morihiro Saito. Quando torna s’indice un’assemblea straordinaria dell’U.I.A. dove si stabilisce di cambiare radicalmente rotta e, da quel momento in poi, di seguire solo gli insegnamenti di Morihiro Saito Sensei e praticare, quello che allora si chiamava, "Takemusu Aiki - Iwama Style".

1985: Primo incontro con Saito Sensei.

Nel 1985, Paolo Corallini invita Saito Sensei, per la prima volta, in Italia. Dal 3 al 10 Febbraio vengono organizzati due seminari; il primo a Torino ed il secondo ad Osimo (AN).

Questa è la prima volta in cui Alessandro Tittarelli incontra personalmente Morihiro Saito Sensei che verrà una seconda volta in Italia nel Maggio 1985, ad Osimo (AN).

Sempre nel 1985 si cambia nome all’associazione: ad "Unione Italiana Aikido" viene aggiunto “Takemusu Aiki Iwama Style Italy: Il Presidente è Paolo Corallini; Alessandro Tittarelli mantiene la carica di Vicepresidente e Gianfranco Leone quella di Segretario Generale. Il nome di quest’associazione, che si trasformerà in un collegio di cinture nere, cambierà ancora varie volte, negli anni a venire, in "Iwama Takemusu Aiki Italy" nel 1992; poi in "Iwama ryu Italy" nel 1998 per poi divenire, dopo la morte di Saito Sensei, nel 2002, l’attuale "Takemusu Aikido Association Italy".

Nel Settembre 1985 Alessandro Tittarelli ed il suo Aiki Shrine Dojo si trasferiscono, dal BUSEN DOJO, presso alcuni locali messi a disposizione dal Dopolavoro Ferroviario, in Via Marconi, 46/a ad Ancona. In questa struttura rimarrà fino al Luglio 2004 (nel 1998 cambierà nome in Aiki Shuren Dojo Ancona) per poi trasferirsi, nel Settembre 2004, in Via Palombare, 55/d, e nel Settembre 2006 nell'attuale indirizzo di via Gervasoni, all'interno della Questura di Ancona.


Il 23 Gennaio 1988 ottiene il 3° Dan (Takemusu Aiki – Iwama Style). Nel Settembre 1990 lascia la direzione del Dojo d’Ancona al suo allievo Paolo Clementi e apre un nuovo corso a Jesi (AN) presso il Palazzetto dello Sport.

1991: Primo incontro con Hitohira Saito Sensei

Il 28 Settembre 1991 ottiene il 4° Dan (Takemusu Aiki – Iwama Style).

Il giorno 11 Novembre 1991 fonda l’Associazione Sportiva "IWAMA RYU", che ha come scopo lo studio, la pratica e lo sviluppo dell’Aikido tradizionale d’Iwama. La prima in Italia ( e forse non solo) a chiamarsi in questo modo. Sono soci fondatori di questa associazione: Alessandro Tittarelli, Paolo Clementi, Giuseppe Lenti, Guerrino Spada, Renzo Tombesi e Angelo Santilli.

Il 13 Novembre 1991 incontra Hitohira Saito Sensei, in occasione del seminario internazionale di Parma (15-16-17 Novembre 1991). E’ la prima volta che Hitohira Sensei tiene un seminario in Europa.

1992: Prima volta in Giappone, Iwama

1996: Riceve da Morihiro Saito Sensei il 5° Dan Ken – Jo

1998: L’"Aiki Shrine Dojo" di Ancona cambia nome in "Aiki Shuren Dojo Ancona"

2000: 1° Gasshuku in Italia di Hitohira Sensei

2002: Ultima visita a Saito Morihiro Sensei

2004: Cerimonia d’inaugurazione ad Iwama, Giappone, della "IWAMA SHIN SHIN AIKI SHURENKAI"

2005: Nasce l’ "Iwama Shin Shin Aiki Shurenkai Italia"

2006: Riceve il 7° Dan, in Iwama, da Kai-cho Saito Hitohira