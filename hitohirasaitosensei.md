---
layout: page
title: Hitohira Saito Sensei
permalink: /hitohirasaitosensei/
---

Hitohira Saito nasce il 12 Febbraio 1957 ad Iwama, nella Prefettura di Ibaraki in Giappone. A 7 anni inizia la pratica dell’Aikido con O Sensei Morirei Ueshiba per proseguire poi, dopo la morte del Fondatore, con il padre, Morihiro Saito Shihan. Termina gli studi tradizionali e si iscrive ad una scuola di cucina per diventare cuoco. Trascorre un primo anno a Sendai e poi altri due anni ad Osaka dove, oltre alla scuola di cucina, ha modo di frequentare il Dojo di Aikido di Seiseki Abe Sensei con il quale studia anche L’Arte della Calligrafia. Nel 1978 termina gli studi e torna ad Iwama dove, con l’aiuto del padre, apre un ristorante. Dopo 7 anni lascia questa attività per dedicarsi completamente alla pratica ed allo studio dell’Aikido. Quando Saito Sensei è lontano da Iwama è Hitohira Sensei che lo sostituisce nella conduzione dell’Aiki Shuren Dojo e nella custodia dell’Aiki Jinjia. Tiene un corso settimanale di Aikido Tradizionale anche a Tokyo, presso lo Yoyogi Uehara Dojo. A partire dal 1986 comincia a dirigere seminari all’estero. Il suo primo Koshukai in Italia risale al 1991, a Parma e da allora la sua presenza nel nostro paese è costante. Verso la fine degli anni ’90, a causa della malattia del padre, si vede Hitohira Sensei sempre più presente nella conduzione del Dojo di Iwama dove praticano sia molti uchi-deshi che soto-deshi.

Nel Dicembre del 2000 accetta l’invito di Alessandro Tittarelli a tenere il primo Gasshuku di Iwama Ryu in Italia, a Filottrano (AN).

Dopo la morte di Saito Sensei, avvenuta nel Maggio 2002, Hitohira Sensei subentra come suo successore. Nell’Ottobre del 2003 restituisce l’Ibaragi Dojo e la custodia dell’Aiki Jinjia alla famiglia Ueshiba prendendo le distanze dalla Fondazione Aikikai. Nel Febbraio del 2004 fonda l’Iwama Shin Shin Aiki Shurenkai con l’obbiettivo di preservare e diffondere le tecniche e la metodologia del Fondatore basata sull’equivalenza del Tai-jutsu, del Jo e del Ken. Attualmente continua la tradizione del padre seguitando ad insegnare regolarmente presso il Tanrenkan Dojo di Iwama e tenendo seminari di "Dentoo Iwama Ryu" in giro per il mondo.

Hitohira Saito Sensei è anche artista e scultore; è sposato ed ha 5 figli.
