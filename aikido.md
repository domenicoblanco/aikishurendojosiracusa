---
layout: page
title: Aikido
permalink: /aikido/
---

Letteralmente quindi "la Via dell’Armonia dello Spirito".

L’Aikido è considerato in Giappone come l’esempio più evoluto di arte marziale e pur attingendo dalle tradizionali arti di combattimento (ju-jutsu, ken-jutsu ecc…) le sue tecniche sono state elaborate e concepite per essere straordinariamente efficaci e al contempo intrinsecamente non violente.

Lo studio di quest’arte è mirato a neutralizzare qualunque tipo di attacco portato a mano nuda o con l’uso di armi (pugnale, bastone, spada) e a liberarsi da ogni possibile presa eseguita frontalmente o da dietro sia sul corpo che sul vestiario; senza mai usare la forza fisica, bensì mettendo in pratica una serie di tecniche che consentono di rendere precario l’equilibrio dell’aggressore anche operando sul controllo delle articolazioni in modo da immobilizzarlo o allontanarlo mediante una proiezione. Le tecniche sono complesse e richiedono pertanto disciplina e concentrazione.

Pur richiedendo un certo impegno fisico la pratica dell’Aikido è adatta ad ogni tipo di persona indipendentemente dal sesso e dall’età.

Le lezioni si svolgono in una sala (dojo) in cui l’area di allenamento è attrezzata con speciali tappeti (tatami) la cui funzione è quella di attutire l’impatto delle cadute. A parteciparvi è tutto il gruppo dei praticanti che seguono l’insegnante senza alcuna distinzione di grado, età, sesso o peso. I principianti indossano un keikogi bianco che è costituito da un paio di pantaloni e da una casacca molto resistente all’usura e alle prese; gli esperti, cinture nere, in aggiunta indossano l’hakama nera, un indumento tradizionale giapponese composto da una speciale gonna pantalone.

Lo studio si sviluppa in modo progressivo e graduale partendo da semplici prese fino a difendersi dall’attacco contemporaneo di più avversari. Un costante lavoro di coppia, svolto tra i praticanti, consente l’apprendimento reciproco in quanto alternativamente si cambiano i ruoli : chi si difende (tori) e chi attacca subendo la tecnica (uke). Col tempo il livello di conoscenza raggiunto è contraddistinto da una serie di gradi, attribuiti dopo il superamento di esami.

Nella pratica dell’Aikido non c’è niente da dimostrare, non c’è dunque spazio per l’aggressione e la violenza; non c’è mai rivalità ma soltanto l’apprendimento reciproco: colui che è attaccato impara ad utilizzare la propria energia mentre chi attacca impara a non essere ferito, in quanto la ferita sarebbe il risultato inevitabile di una resistenza ostinata.

Benchè l’Aikido possa essere inteso come forma scientifica di difesa personale esso è soprattutto un metodo per sviluppare armoniosamente il sistema di coordinamento fra corpo, mente e spirito essendo l’allenamento fisico soltanto un simbolismo, orientato verso un fine morale molto più elevato.



AI-KI-DO

AI – Oltre ad "unione" significa anche "amore", "armonia" con le leggi della natura. Apprendere l’Aikido significa comprendere le leggi naturali del movimento. Colui che pratica è chiamato ad un intenso lavoro teso alla costante ricerca dell’armonizzazione (awase) con l’azione del proprio partner (uke). Le tecniche quindi si ispirano a principi di filosofia naturalistica (il vortice dell’aria, l’onda del mare ecc…) senza alcun contrasto o collisione con la potenza dell’attacco. Si comprenderà allora che la forza viene vinta non opponendosi ma adeguandosi all’azione che irrompe. Mettendo in sintonia il proprio movimento con quello dell’aggressore è possibile dirigere l’energia negativa dello stesso in maniera appropriata neutralizzandola e senza causare danni fisici.

KI – Significa "energia vitale" quindi non solo energia fisica ma anche energia psichica e mentale. Il fine dell’Aikido è di prendere coscienza della propria energia e di imparare a controllarla. Questa energia è la stessa che riempie l’Universo e che nell’individuo è la misura della sua vitalità : quando non c’è "Ki" l’immobilità e la morte sopraggiungono. Tramite le tecniche dell’Aikido è possibile migliorare la propria salute fisica, acquisire un particolare autocontrollo ed una straordinaria energia interna, mantenendo la scioltezza giovanile e la serenità interiore.

DO – Significa "Via" o cammino ma inteso come percorso e ricerca spirituale da compiere.

L'Aikido è quindi il percorso spirituale che ci permette di unificare la nostra energia interna in connessione con la sorgente universale per poi espanderla sulla terra con il fine di diventare UNO con tutto ciò che ci circonda.

Nell’Aikido questo percorso avviene attraverso quattro livelli di consapevolezza:

Nel primo livello KO -TAI (che corrisponde a "corpo solido") l’obiettivo è di saper padroneggiare il proprio corpo in maniera efficace e corrisponde all’acquisizione pratica della tecnica attraverso un attento studio eseguito passo dopo passo degli angoli di squilibrio e facendo la massima attenzione alla precisione dei movimenti.

Nel secondo livello JU -TAI (che corrisponde a "corpo flessibile") l’esecuzione delle tecniche comincia ad essere flessibile: il corpo comincia a muoversi con disinvoltura e senza incertezze, si comincia a percepire nella tecnica il connubio tra chi attacca e chi si difende.

Nel terzo livello RYU -TAI (che corrisponde a "corpo fluido") si studia il momento in cui si fondono l’attacco e la difesa: l’esecuzione della tecnica diventa fluida ed in armonia con l’azione di attacco, non vi sono più pause o momenti di interruzione dell’azione, si dirige l’energia dell’avversario in modo continuativo facendola scorrere nella direzione appropriata.

Nel quarto livello KI -TAI (che corrisponde a "livello spirituale") si raggiunge la consapevolezza del momento in cui nasce l’attacco. Questo livello rappresenta la meta più alta a cui un Aikidoka può arrivare e vi giunge solo dopo un lungo percorso frutto di un continuo esercizio; esso rappresenta il culmine fino quasi a sfiorare la perfezione in quanto esiste la completa unione non solo fisica e mentale, ma soprattutto spirituale tra "tori" e "uke" Il corpo non più vincolato nei suoi movimenti e la mente in completa sintonia con quella dell’avversario lasciano il posto allo spirito che può ora agire liberamente senza più vincoli di sorta.

L’Aikido compie un salto in avanti rispetto alle altre arti marziali poiché, senza perdere in efficacia, diventa un’Arte Spirituale. Le tecniche di difesa rappresentano infatti il "mezzo" per conoscere se stessi e comprendere come vivere: vincere o perdere non rappresenta la giusta Via alla conoscenza in quanto chi desidera essere imbattibile conserva uno spirito bellicoso e mantiene la propria aggressività. Vincere veramente significa sconfiggere lo spirito di discordia del nostro animo perché ciò che desideriamo o cerchiamo di fuggire si trova dentro di noi. La vera forza sta nell’acquisire una forza interiore, mantenendo sempre la giusta calma e serenità dello spirito ma non per questo essere deboli, bensì decisi e determinati nelle nostre azioni, sicuri delle scelte operate e sempre pronti ad affrontare ogni nuova situazione con la giusta grinta.

Chi pratica l’Aikido acquisisce con il tempo un determinato atteggiamento sia fisico che mentale che si riflette nella vita di tutti i giorni soprattutto al di fuori del "dojo". Nella pratica con il compagno la prassi del saluto, il ringraziamento, il ripetere gli esercizi alternativamente e le tecniche eseguite sempre nel rispetto della sua integrità fisica accrescono il senso di rispetto per la natura e per il prossimo. Il raggiungimento della corretta esecuzione della tecnica porta ad essere sempre precisi ed accurati in tutti i campi. La ricerca dell’armonizzazione, possibile solo attraverso l’autocorrezione, sviluppa il proprio senso critico. L’accettazione dei consigli da parte dei compagni più anziani (sempai) porta ad avere sempre la giusta dose di umiltà. Tutti questi costituiscono fattori determinanti per un profondo miglioramento della propria personalità. La pratica dell’Aikido quando è basata sulla ricerca dell’armonizzazione, della tecnica appropriata alla situazione contingente e sul rispetto del partner consente di cercare la strategia giusta per raggiungere il proprio scopo. Questo è anche un modello di comportamento applicabile nella vita sociale: anche qui, a volte, è necessario sapersi muovere con discrezione cercando sempre la strada giusta per conseguire i propri intenti mantenendo il rispetto per il prossimo.

L’Aikido, dunque, non è soltanto una attività fisica, quale appare a prima vista, ma è un modo meraviglioso per "vivere" la nostra esistenza.



Perché Aikido Iwama Ryu?
Oggi nel mondo ci sono ancora molti maestri, anche se di una certa età, che sono stati allievi diretti del Fondatore. Ognuno di loro ha appreso questa "arte" in base al tempo trascorso insieme e secondo la propria capacità di interpretare le tecniche che "O Sensei Morihei Ueshiba" mostrava loro: dobbiamo tener presente che la pedagogia di quel periodo imponeva agli allievi di essere molto vigili ed attenti a quello che il Maestro mostrava e diceva loro, senza però avere la possibilità di fare domande o chiedere spiegazioni, alle quali bisognava arrivare attraverso il proprio cammino esperienziale. Il risultato è che oggi ci troviamo di fronte tanti maestri che insegnano secondo uno stile personale, da cui derivano i differenti modi di praticare l’Aikido.
Iwama è il nome di una piccola cittadina giapponese a nord di Tokyo nella prefettura di "Ibaraki" dove il Fondatore si era trasferito durante la seconda guerra mondiale e dove era rimasto fino alla sua morte avvenuta il 26 aprile 1969. Qui "O Sensei" (il "Grande Maestro") aveva costruito il suo "Aiki Shuren Dojo" (letteralmente: il dojo infernale dell’aiki; dove infernale sta per duro, severo) attigua alla propria abitazione e poche decine di metri di distanza, aveva eretto l’Aiki Jinjia, il santuario shintoista dedicato all’Aikido, oggi punto di riferimento e meta per tutti i praticanti dell’arte. In questo luogo tranquillo e immerso nella natura, tra lavori nei campi e studi spirituali, il Fondatore aveva trovato le condizioni ideali per elaborare la sua "arte" che aveva chiamato "Takemusu Aiki" conosciuta nel mondo con il nome di "Aikido"

"Take" significa "tecnica marziale", "musu" sta per "sgorgare, nascere spontaneamente", "ai" equivale ad "unione" e "ki" è "spirito" Quando si è in armonia con se stessi e gli altri (aiki) la tecnica marziale (take) nasce spontaneamente (musu).

Nel 1946 entrò nel dojo di Iwama , come uchi-deshi (allievo interno, che vive nel dojo) il giovane Morihiro Saito; egli aveva appena 18 anni e rimase sempre accanto al Fondatore per i 23 anni successivi, aiutandolo nei lavori del dojo e dei campi .

In tutti questi anni il Maestro Morihiro Saito ebbe modo di vedere ed apprendere l’Aikido durante tutto il percorso di elaborazione che "O Sensei" fece quotidianamente, memorizzando la stessa metodologia di insegnamento basata sul mostrare i vari movimenti, spiegandoli con dei "kuden" (letteralmente "insegnamenti verbali") che in maniera metaforica contenevano l’essenza ed il significato della tecnica.

Alla morte del Fondatore, nel 1969, Morihiro Saito Shihan divenne il responsabile del Dojo di Iwama (Ibaraki Dojo, conosciuto come Aiki Shuren Dojo), custode della casa del Fondatore e dell’Aiki Jinjia (il santuario dove ogni mese si tiene una cerimonia e dove una volta all’anno, il 29 Aprile, si ritrovano tutti gli Aikidoka del Giappone e del mondo per una grande commemorazione).

Egli insegnò regolarmente tutti i giorni, sia agli uchi-deshi che ai soto-deshi dell’Aiki Shuren Dojo di Iwama e tenne Seminari di "Aikido Iwama Ryu" in tutto il mondo contribuendo enormemente alla diffusione dell’Aikido tradizionale del Fondatore.

"Ryu" significa scuola e quindi "IWAMA RYU" è la "scuola di Iwama" con la quale vogliamo evidenziare l’Aikido originario del Fondatore in modo da contraddistinguerlo da qualsiasi altro "stile personale".

Alla morte di Morihiro Saito Soke, avvenuta il 13 Maggio 2002, è subentrato suo figlio Hitohiro Saito Soke. Successore ufficiale e caposcuola (Soke) il Maestro Hitohiro Saito continua la tradizione del padre seguitando ad insegnare regolarmente in Iwama e tenendo seminari di "Aikido Iwama Ryu" in giro per il mondo. Per suggellare questo intento, nell’Ottobre del 2003, Hitohiro Saito Soke ha deciso di lasciare la Fondazione Aikikai e divenire indipendente con il nome di "IWAMA SHINSHIN AIKI SHUREN KAI".